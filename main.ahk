﻿; Download Script: https://gitlab.com/stand1service/so2script

#SingleInstance, force

; Переменные
pathEmulator = "E:\MuMuPlayerGlobal-12.0\shell\MuMuPlayer.exe" ; путь до эмулятора
pathTrainer = "C:\Users\dmitry\Desktop\lib\trainer.CETRAINER" ; путь до CETrainer
titleEmulator = Бот ; заголовок эмулятора
titleTrainer = CETrainer ; заголовок CETrainer
xTrainer = 313 ; расстояние между окнами CETrainer по горизонтали
yTrainer = 508 ; высота, на которой будут расположены CETrainer
numEmulators = 10 ; количество эмуляторов
numTrainers = 6 ; количество CETrainer
wait = 2000 ; время ожидания между запусками

^1:: ; CTRL + 1: Запуск эмуляторов и выравнивание CETrainer
    start_all(pathEmulator, pathTrainer, titleTrainer, xTrainer, yTrainer, wait)
Return

^2:: ; CTRL + 2: Запуск игры
    Loop, %numEmulators%{
        Run, %pathEmulator% -p com.axlebolt.standoff2 -v %A_Index%
    }
Return

^3:: ; CTRL + 3: Закрытие всех процессов
    Loop, %numEmulators%{
        WinClose, %titleEmulator%
    }

    Loop, %numTrainers%{
        WinClose, %titleTrainer%
    }
ExitApp

start_all(pathEmulator, pathTrainer, titleTrainer, xTrainer, yTrainer, wait) {
    Loop, 6 {
        Run, %pathEmulator% -v %A_Index%
        Sleep, %wait%
        Run, %pathTrainer%
        Sleep, %wait%
    }

    emulatorCount = 7

    Loop, 4 {
        Run, %pathEmulator% -v %emulatorCount%
        Sleep, %wait%
        emulatorCount++
    }

    WinGetPos, , , Width, , %titleTrainer%
    WinGet, idList, List, %titleTrainer%

    X := A_ScreenWidth - Width

    Loop, %idList% {
        thisID := idList%A_Index%
        WinMove, ahk_id %thisID%, , %X%, %yTrainer%
        X -= xTrainer
    }
}